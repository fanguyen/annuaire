import React, { Component } from "react";
import "./App.css";
import md5 from "md5";
// import _ from "lodash";
import _orderBy from "lodash/orderBy";
import FaStar from "react-icons/lib/fa/star";
import FaEnvelop from "react-icons/lib/fa/envelope";

class Headfilter extends Component {
  isActiveFilter = filter => {
    return this.props.filter === filter ? "btn-primary" : "btn-secondary";
  };
  render() {
    return (
      <div className="col-lg-12 header-bar-filter">
        <input
          className={`btn col-lg-4 ${this.isActiveFilter("all")}`}
          type="button"
          value="All"
          onClick={() => this.props.handleFilterChange("all")}
        />

        <input
          className={`btn col-lg-4 ${this.isActiveFilter("male")}`}
          type="button"
          value="Male"
          onClick={() => this.props.handleFilterChange("male")}
        />

        <input
          className={`btn col-lg-4 ${this.isActiveFilter("female")}`}
          type="button"
          value="Female"
          onClick={() => this.props.handleFilterChange("female")}
        />
      </div>
    );
  }
}

class Createuser extends Component {
  constructor(props) {
    super(props);
    this.initialState = {
      first: "",
      last: "",
      email: ""
    };
    this.state = this.initialState;
  }

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  addUser = () => {
    this.props.addUser(this.state);
    this.setState(this.initialState);
  };

  render() {
    return (
      <div className="col-md-12 header-bar-add ">
        <input
          className="col-md-3"
          type="text"
          name="first"
          value={this.state.first}
          placeholder="First Name"
          onChange={this.handleChange}
        />
        <input
          className="col-md-3"
          type="text"
          name="last"
          value={this.state.last}
          placeholder="Last Name"
          onChange={this.handleChange}
        />
        <input
          className="col-md-3"
          type="text"
          name="email"
          value={this.state.email}
          placeholder="email"
          onChange={this.handleChange}
        />
        <input
          className="btn btn-primary col-md-3"
          type="button"
          value="OK"
          onClick={this.addUser}
        />
      </div>
    );
  }
}

class User extends Component {
  constructor(props) {
    super(props);
    this.state = { edit: false, delete: false };
    this.inputRefFirst = React.createRef();
    this.inputRefLast = React.createRef();
  }

  toggleEdit = () => {
    console.log("click");
    this.setState({ edit: !this.state.edit });
  };

  toggleDelete = () => {
    console.log("delete");
  };

  saveName = () => {
    this.props.handleSaveName(
      this.props.pers.id,
      this.inputRefFirst.current.value,
      this.inputRefLast.current.value
    );
    this.toggleEdit();
  };

  isFavorite = () => {
    console.log(this.props.favorite);
    return this.props.favorite === this.props.pers.id ? "gold" : "gray";
  };

  makeFavorite = () => {
    this.props.makeFavorite(this.props.pers.id);
  };
  render() {
    const { pers } = this.props;
    return (
      <div className={`user-box col-lg-3 `}>
        <div className="user-container">
          <FaStar
            className="fa-star"
            onClick={this.makeFavorite}
            color={this.isFavorite()}
          />
          {pers.picture ? (
            <div className="image-container">
              <img className="user-image" src={pers.picture.large} alt="" />
            </div>
          ) : null}

          {this.state.edit ? (
            <div>
              <input
                type="text"
                defaultValue={pers.name.first}
                ref={this.inputRefFirst}
              />
              <input
                type="text"
                defaultValue={pers.name.last}
                ref={this.inputRefLast}
              />
            </div>
          ) : (
            <div className="user-content-name">
              <span>{pers.name.first}</span> <span>{pers.name.last}</span>
            </div>
          )}
          <br />
          <div className="user-content">
            <div className="fa-envelope" />
            <a href="" target="_blank">
              <span className="user-content">{pers.email}</span>
            </a>
          </div>
          <br />

          {this.state.edit ? (
            <div>
              <input
                className="edit-button"
                type="button"
                value="Cancel"
                onClick={this.toggleEdit}
              />
              <input
                className="edit-button"
                type="button"
                value="Delete"
                onClick={() => this.props.handleDelete(pers.id)}
              />

              <input
                className="edit-button"
                type="button"
                value="Save"
                onClick={this.saveName}
              />
            </div>
          ) : (
            <input
              className="edit-button"
              type="button"
              value="Edit"
              onClick={this.toggleEdit}
            />
          )}
          <br />
        </div>
      </div>
    );
  }
}

class Userlist extends Component {
  sortByName = list => {
    return _orderBy(list, ["name.first", "name.last"], ["asc", "asc"]);
  };

  render() {
    const filteredPersons = this.props.persons.filter(
      pers => this.props.filter === "all" || pers.gender === this.props.filter
    );
    const persons = this.sortByName(filteredPersons);
    return persons.map(pers => (
      <User
        key={pers.id}
        pers={pers}
        handleDelete={id => this.props.handleDelete(id)}
        handleSaveName={(id, firstName, lastName) =>
          this.props.handleSaveName(id, firstName, lastName)
        }
        favorite={this.props.favorite}
        makeFavorite={this.props.makeFavorite}
      />
    ));
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      persons: [],
      filter:
        localStorage.getItem("filter") !== null
          ? localStorage.getItem("filter")
          : "all",
      favorite:
        localStorage.getItem("fav") !== null
          ? localStorage.getItem("fav")
          : null
    };
  }

  componentDidMount() {
    fetch("https://randomuser.me/api/?results=30")
      .then(results => {
        // results
        // this.setState({ results: results });
        return results.json();
      })
      .then(data => {
        const persons = data.results.map(pers => {
          return {
            ...pers,
            id: md5(pers.name.first + pers.name.last)
          };
        });
        this.setState({ persons });
      });
  }

  handleFilterChange = filter => {
    this.setState({ filter });
    console.log(this.state);
    localStorage.setItem("filter", filter);
  };

  handleSaveName = (id, first, last) => {
    console.log(id);
    console.log(first);

    const persons = this.state.persons.map(pers => {
      if (pers.id === id) {
        return {
          ...pers,
          name: {
            ...pers.name,
            first,
            last
          }
          //   status: todo.status === "completed" ? "active" : "completed"
        };
      }
      return pers;
    });

    this.setState({ persons });
  };

  handleDelete = id => {
    const persons = this.state.persons.filter(pers => pers.id !== id);
    console.log(id);
    this.setState({ persons });
  };

  addUser = user => {
    let persons = this.state.persons;
    persons.push({
      name: {
        first: user.first,
        last: user.last
      },
      email: user.email,
      id: md5(user.first + user.last)
    });
    this.setState({ persons });
    console.log(user);
  };

  makeFavorite = favorite => {
    localStorage.setItem("fav", favorite);
    this.setState({ favorite });
  };

  render() {
    return (
      <div className="col-lg-12">
        <ul className="thumbnail">
          <Createuser addUser={this.addUser} />
          <Headfilter
            filter={this.state.filter}
            handleFilterChange={this.handleFilterChange}
          />
          {this.state.persons.length > 0 ? (
            <Userlist
              makeFavorite={this.makeFavorite}
              favorite={this.state.favorite}
              persons={this.state.persons}
              filter={this.state.filter}
              handleSaveName={this.handleSaveName}
              handleDelete={this.handleDelete}
            />
          ) : null}
        </ul>
      </div>
    );
  }
}

export default App;
